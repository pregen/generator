import { selector } from "./selector";
import { roll } from "./roll";
import { random } from "./random";
import { stats } from "../stats/all";

export let character = {

    generate: function () {

        const char = {
            race: selector.fromCollection(stats.races),
            background: selector.fromCollection(stats.backgrounds),
            gender: selector.fromCollection(stats.genders),
            stats: { raw: roll.attributes() }
        }

        char.class = generateClass(char.race);
        char.alignment = generateAlignment(char);
        char.age = generateAge(char.race.ages);
        char.name = generateName(char.race, char.gender);
        char.order = sortWeightedStats(char);
        char.stats.calc = produceStats(char);

        return char;
    }

};

function generateClass(race) {
    
    // build weights
    const weights = {};
    for (let className in stats.classes) {
        weights[className] = { 'class': stats.classes[className], weight: 0 };
    };

    // add racial weights to classes
    race.classes.forEach(function (c) {
        const className = c.name.replace('-', '');
        if (weights[className] != null) {
            weights[className].weight += c.weight;
        };
    });

    // choose and return the weighted class
    return selector.fromCollection(weights).class;

}

function generateAlignment(char) {

    // generate moral alignment
    let weights = [1, 1, 1];
    for (let i = 0; i < 3; i++) {
        if (char.race.alignments.moral[i] != null) weights[i] += char.race.alignments.moral[i];
        if (char.class.alignments.moral[i] != null) weights[i] += char.class.alignments.moral[i];
    }
    const morals = [{ name: 'Good', weight: weights[0] }, { name: 'Neutral', weight: weights[1] }, { name: 'Evil', weight: weights[2] }];
    const moral = selector.fromCollection(morals);

    // generate societal alignment
    weights = [1, 1, 1];
    for (let i = 0; i < 3; i++) {
        if (char.race.alignments.societal[i] != null) weights[i] += char.race.alignments.societal[i];
        if (char.class.alignments.societal[i] != null) weights[i] += char.class.alignments.societal[i];
    }

    const societals = [{ name: 'Lawful', weight: weights[0] }, { name: 'Neutral', weight: weights[1] }, { name: 'Chaotic', weight: weights[2] }];
    const societal = selector.fromCollection(societals);

    // generate the alignment string
    return { moral: moral, societal: societal, name: (moral.name == societal.name) ? moral.name : societal.name + ' ' + moral.name };
}

function generateAge(ages) {
    const age = selector.fromCollection(ages);
    return { years: random.between(age.min, age.max), group: age.name };
}

function generateName(race, gender) {

    // get the appropriate names collection
    const raceName = race.name.replace('-', '');
    const names = race.names;

    // build the name from the name sources
    let name = '';
    for (let i = 0; i < names[gender.letter].length; i++) name += selector.fromCollection(names[gender.letter][i]);
    return name;

}

function removeItemFromArray(collection, item) {
    let index = -1;
    for (let i in collection) {
        if (collection[i] == item) {
            index = i;
        }
    }
    if (index != -1) {
        collection.splice(index, 1);
    }
    return collection;
}

function sortWeightedStats(char) {
    let listToTrim = [];
    for (let i in char.race.stats) {
        const stat = char.race.stats[i];
        const bonus = ('bonus' in stat) ? stat.bonus : 0;
        const classStat = char.class.stats[i];
        const classBonus = ('bonus' in classStat) ? classStat.bonus : 0;
        const item = { name: stat.stat, weight: stat.weight * classStat.weight, bonus: bonus + classBonus };
        listToTrim.push(item);
    }
    let results = [];

    for (let i = 0; i < char.race.stats.length; i++) {
        const item = selector.fromCollection(listToTrim);
        listToTrim = removeItemFromArray(listToTrim, item);
        results.push({ name: item.name, bonus: item.bonus });
    }

    return results;
}

function produceStats(char) {

    let result = [];

    for (let i in char.order) {
        const stat = char.order[i];
        const statName = stat.name;
        const roll = char.stats.raw.stats[i].total;
        const bonus = ('bonus' in stat) ? stat.bonus : 0;
        const points = char.stats.raw.stats[i].points;
        result[statName] = { name: statName, roll: roll, bonus: bonus, points: points, score: roll + bonus };
    }

    return [
        { name: "str", score: result["str"] },
        { name: "dex", score: result["dex"] },
        { name: "con", score: result["con"] },
        { name: "int", score: result["int"] },
        { name: "wis", score: result["wis"] },
        { name: "cha", score: result["cha"] }
    ];

}