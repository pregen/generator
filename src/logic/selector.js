import { random } from "./random";

export let selector = {

    fromCollection: function(collection) {
        
        const weightAttribute = 'weight';

        // force ourselves to work with an array
        let items = [];
        for (let c in collection) {
            const item = collection[c];
            const weight = item[weightAttribute] != null ? item[weightAttribute] : 10;
            items.push({ item: item, weight: weight });
        }

        // assign each item a maximum number
        let rangeMax = 0;
        for (let i = 0; i < items.length; i++) {
            rangeMax += items[i].weight;
            items[i].max = rangeMax;
        }

        // define the range
        const rangeMin = 1;

        // select an random number in the range
        const selectedNum = random.between(rangeMin, rangeMax);
        for (let i in items) if (items[i].max >= selectedNum) return items[i].item;

        // return null if something went wrong
        return null;
    }

}