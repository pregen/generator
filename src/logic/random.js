export let random = {
    
    number: function(num_digits) {

        // initialize needed variables
        let seed = 0;
        if (window.rpg == null) window.rpg = {};

        // fetch a new seed
        if (window.rpg.seed != null) {
            seed = window.rpg.seed;
        } else {
            seed = randomDigits(num_digits);
        }

        // create a new random number by XOR with the seed
        let newNum = randomDigits(num_digits) ^ seed;

        // if it's negative, invert it
        if (newNum < 0) newNum *= -1;

        // set the new number as our new seed and return it
        window.rpg.seed = newNum;
        return newNum;
    },
    
    between: function(min, max) {

        // figure out how much space is between the min and max
        const range = (max > min) ? max - min + 1 : min - max + 1;
        
        // get a new, random 10-digit number
        const rand = Math.round(this.number(10));

        // reduce this to a number between the min and max
        return min + (rand % range);
    }
    
}


function randomDigits(num_digits) {
    // get a random number between 0 and 1
    let rand = Math.random();
    // multiply by 10 for each digit we'd like
    for (let x = 0; x < num_digits; x++) { rand *= 10; }
    // truncate off everything after the decimal and return
    return Math.floor(rand);
}