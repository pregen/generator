import { random } from "./random";

export let roll = {
    
    d6: function() {
        return random.between(1, 6);
    },
    
    stat: function() {
        
        // roll 4 d6 & sort lowest-to-highest
        let rolls = [ this.d6(), this.d6(), this.d6(), this.d6() ];
        rolls.sort();
        const total = rolls[1] + rolls[2] + rolls[3]; 

        // return the stat as rolled
        return {
            total: total,
            rolls: rolls,
            points: statPoints(total)
        }
        
    },
    
    attributes: function() {
        
        // roll 6 stats
        let stats = [this.stat(), this.stat(), this.stat(), this.stat(), this.stat(), this.stat() ];
        
        // sort them highest-to-lowest
        stats.sort((a,b) => b.total - a.total);

        if (!validateAttributes(stats)) return this.attributes();

        return {
            stats: stats,
            points: stats.reduce((a,b) => a+b.points, 0)
        }
    }
}


function validateStat(stat) {
    return true;
}

function validateAttributes(attributes) {

    // REMEMBER: the dice rolls are numbered from 0 (highest) to 5 (lowest)

    // reroll a new set if the highest is too low (less than 13)
    if (attributes[0].total < 13) return false;
    // reroll a new set if the second-highest is too high (more than 15)
    if (attributes[1].total > 15) return false;
    // reroll a new set if the third-highest is too high (over 14)
    if (attributes[2].total > 14) return false;
    // reroll if the third-lowest is too low (under 12)
    if (attributes[3].total < 12) return false;
    // reroll a new set if the second-lowest is under 10
    if (attributes[4].total < 10) return false;
    // reroll a new set if the lowest is under 6
    if (attributes[5].total < 6) return false;
    // reroll a new set if the lowest is over 10
    if (attributes[5].total > 10) return false;

    // stay within a certain point-buy score
    const score = attributes.reduce((a, b) => a + b.points, 0);
    if (score < 25) return false;
    if (score > 32) return false;

    return true;
}

function statPoints(statTotal) {
    switch(statTotal)
    {
        case 3:
        return -5;
        case 4:
        return -4;
        case 5: 
        return -3;
        case 6:
        return -2;
        case 7:
        return -1;
        case 8:
        return 0;
        case 9:
        return 1;
        case 10:
        return 2;
        case 11:
        return 3;
        case 12:
        return 4;
        case 13:
        return 5;
        case 14:
        return 7;
        case 15:
        return 9;
        case 16:
        return 11;
        case 17: 
        return 14;
        case 18:
        return 17;
        default:
        return 100;
    }
}