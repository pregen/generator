export let Gnome = {

    name: 'Gnome',
    weight: 10,

    ages: [
        { name: "Young Adult", min: 15, max: 30, weight: 20 },
        { name: "Adult", min: 31, max: 150, weight: 60 },
        { name: "Middle Aged", min: 151, max: 350, weight: 17 },
        { name: "Elderly", min: 351, max: 450, weight: 3 }
    ],

    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 3 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 6, bonus: 2 },
        { stat: "wis", weight: 2 },
        { stat: "cha", weight: 2 }
    ],

    classes: [
        { name: "Barbarian", weight: 1 },
        { name: "Bard", weight: 10 },
        { name: "Cleric", weight: 10 },
        { name: "Druid", weight: 10 },
        { name: "Fighter", weight: 1 },
        { name: "Monk", weight: 10 },
        { name: "Paladin", weight: 1 },
        { name: "Ranger", weight: 7 },
        { name: "Rogue", weight: 10 },
        { name: "Sorcerer", weight: 8 },
        { name: "Warlock", weight: 5 },
        { name: "Wizard", weight: 10 }
    ],

    alignments: { 
        moral: [25, 8, 1], 
        societal: [15, 3, 15] 
    },

    names: {
        m: [
            ["Al", "Ari", "Bil", "Bri", "Cal", "Cor", "Dav", "Dor", "Eni", "Er", "Far", "Fel", "Ga", "Gra", "His", "Hor", "Ian", "Ipa", "Je", "Jor", "Kas", "Kel", "Lan", "Lo", "Man", "Mer", "Nes", "Ni", "Or", "Oru", "Pana", "Po", "Qua", "Quo", "Ras", "Ron", "Sa", "Sal", "Sin", "Tan", "To", "Tra", "Um", "Uri", "Val", "Vor", "War", "Wil", "Wre", "Xal", "Xo", "Ye", "Yos", "Zan", "Zil"],
            ["bar", "ben", "bis", "corin", "cryn", "don", "dri", "fan", "fiz", "gim", "grim", "hik", "him", "ji", "jin", "kas", "kur", "len", "lin", "min", "mop", "morn", "nan", "ner", "ni", "pip", "pos", "rick", "ros", "rug", "ryn", "ser", "ston", "tix", "tor", "ver", "vyn", "win", "wor", "xif", "xim", "ybar", "yur", "ziver", "zu"]
        ],
        f: [
            ["Alu", "Ari", "Ban", "Bree", "Car", "Cel", "Daphi", "Do", "Eili", "El", "Fae", "Fen", "Fol", "Gal", "Gren", "Hel", "Hes", "Ina", "Iso", "Jel", "Jo", "Klo", "Kri", "Lil", "Lori", "Min", "My", "Ni", "Ny", "Oda", "Or", "Phi", "Pri", "Qi", "Que", "Re", "Rosi", "Sa", "Sel", "Spi", "Ta", "Tifa", "Tri", "Ufe", "Uri", "Ven", "Vo", "Wel", "Wro", "Xa", "Xyro", "Ylo", "Yo", "Zani", "Zin"],
            ["bi", "bys", "celi", "ci", "dira", "dysa", "fi", "fyx", "gani", "gyra", "hana", "hani", "kasys", "kini", "la", "li", "lin", "lys", "mila", "miphi", "myn", "myra", "na", "niana", "noa", "nove", "phina", "pine", "qaryn", "qys", "rhana", "roe", "sany", "ssa", "sys", "tina", "tra", "wyn", "wyse", "xi", "xis", "yaris", "yore", "za", "zyre"]
        ]
    }

}