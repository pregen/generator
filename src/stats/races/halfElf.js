export let HalfElf = {
    
    name: 'Half-Elf',
    weight: 15,
    
    ages: [
        { name: "Adolescent", min: 14, max: 20, weight: 5 },
        { name: "Adult", min: 21, max: 120, weight: 90 },
        { name: "Elderly", min: 121, max: 150, weight: 5 }
    ],
    
    stats: [
        { stat: "str", weight: 3, bonus: 1 },
        { stat: "dex", weight: 2 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 1 },
        { stat: "wis", weight: 4, bonus: 1 },
        { stat: "cha", weight: 6, bonus: 2 }
    ],
    
    classes: [
        { name: "Barbarian", weight: 1 },
        { name: "Bard", weight: 5 },
        { name: "Cleric", weight: 7 },
        { name: "Druid", weight: 10 },
        { name: "Fighter", weight: 7 },
        { name: "Monk", weight: 7 },
        { name: "Paladin", weight: 1 },
        { name: "Ranger", weight: 10 },
        { name: "Rogue", weight: 5 },
        { name: "Sorcerer", weight: 5 },
        { name: "Warlock", weight: 1 },
        { name: "Wizard", weight: 10 }
    ],
    
    alignments: { 
        moral: [15, 10, 8], 
        societal: [1, 10, 25]
    },
    
    names: {
        m: [
            ["Al", "Aro", "Bar", "Bel", "Cor", "Cra", "Dav", "Dor", "Eir", "El", "Fal", "Fril", "Gaer", "Gra", "Hal", "Hor", "Ian", "Ilo", "Jam", "Kev", "Kri", "Leo", "Lor", "Mar", "Mei", "Nil", "Nor", "Ori", "Os", "Pan", "Pet", "Quo", "Raf", "Ri", "Sar", "Syl", "Tra", "Tyr", "Uan", "Ul", "Van", "Vic", "Wal", "Wil", "Xan", "Xav", "Yen", "Yor", "Zan", "Zyl"],
            ["avor", "ben", "borin", "coril", "craes", "deyr", "dithas", "elor", "enas", "faelor", "faerd", "finas", "fyr", "gotin", "gretor", "homin", "horn", "kas", "koris", "lamir", "lanann", "lumin", "minar", "morn", "nan", "neak", "neiros", "orin", "ovar", "parin", "phanis", "qarim", "qinor", "reak", "ril", "ros", "sariph", "staer", "torin", "tumil", "valor", "voril", "warith", "word", "xian", "xiron", "yeras", "ynor", "zaphir", "zaren"]
        ],
        f: [
            ["Alu", "Aly", "Ar", "Bren", "Byn", "Car", "Co", "Dar", "Del", "El", "Eli", "Fae", "Fha", "Gal", "Gif", "Haly", "Ho", "Ile", "Iro", "Jen", "Jil", "Kri", "Kys", "Les", "Lora", "Ma", "Mar", "Mare", "Neri", "Nor", "Ol", "Ophi", "Phaye", "Pri", "Qi", "Que", "Rel", "Res", "Sael", "Saf", "Syl", "Ther", "Tyl", "Una", "Uri", "Ven", "Vyl", "Win", "Wol", "Xil", "Xyr", "Yes", "Yll", "Zel", "Zin"],
            ["aerys", "anys", "bellis", "bwynn", "cerys", "charis", "diane", "dove", "elor", "enyphe", "faen", "fine", "galyn", "gwynn", "hana", "hophe", "kaen", "kilia", "lahne", "lynn", "mae", "malis", "mythe", "nalore", "noa", "nys", "ona", "phira", "pisys", "qarin", "qwyn", "rila", "rora", "seris", "stine", "sys", "thana", "theris", "tihne", "trana", "viel", "vyre", "walyn", "waris", "xaris", "xipha", "yaries", "yra", "zenya", "zira"]
        ]
    }
    
}