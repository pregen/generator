export let Dragonborn = {

    name: 'Dragonborn',
    weight: 6,
    
    ages: [
        { name: "Adolescent", min: 8, max: 15, weight: 5 },
        { name: "Adult", min: 16, max: 45, weight: 90 },
        { name: "Elderly",     min: 46, max: 65, weight: 5 }
    ],
    
    stats: [
        { stat: "str", weight: 10, bonus: 2 },
        { stat: "dex", weight: 2 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 2 },
        { stat: "wis", weight: 1 },
        { stat: "cha", weight: 6, bonus: 1 }
    ],
    
    classes: [
        { name: "Barbarian", weight: 10 },
        { name: "Bard", weight: 1 },
        { name: "Cleric", weight: 5 },
        { name: "Druid", weight: 1 },
        { name: "Fighter", weight: 20 },
        { name: "Monk", weight: 1 },
        { name: "Paladin", weight: 10 },
        { name: "Ranger", weight: 1 },
        { name: "Rogue", weight: 1 },
        { name: "Sorcerer", weight: 15 },
        { name: "Warlock", weight: 15 },
        { name: "Wizard", weight: 8 }
    ],
    
    alignments: {
        moral: [12, 3, 18],
        societal: [16, 8, 16] 
    },
    
    names: {
        m: [
            ["Ali", "Ar", "Ba", "Bal", "Bel", "Bha", "Bren", "Caer", "Calu", "Dur", "Do", "Dra", "Era", "Faer", "Fro", "Gre", "Ghe", "Gora", "He", "Hi", "Ior", "Jin", "Jar", "Kil", "Kriv", "Lor", "Lumi", "Mar", "Mor", "Med", "Nar", "Nes", "Na", "Oti", "Orla", "Pri", "Pa", "Qel", "Ravo", "Ras", "Rho", "Sa", "Sha", "Sul", "Taz", "To", "Trou", "Udo", "Uro", "Vor", "Vyu", "Vrak", "Wor", "Wu", "Wra", "Wul", "Xar", "Yor", "Zor", "Zra"],
            ["barum", "bor", "broth", "ciar", "crath", "daar", "dhall", "dorim", "farn", "fras", "gar", "ghull", "grax", "hadur", "hazar", "jhan", "jurn", "kax", "kris", "kul", "lasar", "lin", "mash", "morn", "naar", "prax", "qiroth", "qrin", "qull", "rakas", "rash", "rinn", "roth", "sashi", "seth", "skan", "trin", "turim", "varax", "vroth", "vull", "warum", "wunax", "xan", "xiros", "yax", "ythas", "zavur", "zire", "ziros"]
        ],
        f: [
            ["Ari", "A", "Bi", "Bel", "Cris", "Ca", "Drys", "Da", "Erli", "Esh", "Fae", "Fen", "Gur", "Gri", "Hin", "Ha", "Irly", "Irie", "Jes", "Jo", "Ka", "Kel", "Ko", "Lilo", "Lora", "Mal", "Mi", "Na", "Nes", "Nys", "Ori", "O", "Ophi", "Phi", "Per", "Qi", "Quil", "Rai", "Rashi", "So", "Su", "Tha", "Ther", "Uri", "Ushi", "Val", "Vyra", "Welsi", "Wra", "Xy", "Xis", "Ya", "Yr", "Zen", "Zof"],
            ["birith", "bis", "bith", "coria", "cys", "dalynn", "drish", "drith", "faeth", "fyire", "gil", "gissa", "gwen", "hime", "hymm", "karyn", "kira", "larys", "liann", "lyassa", "meila", "myse", "norae", "nys", "patys", "pora", "qorel", "qwen", "rann", "riel", "rina", "rinn", "rish", "rith", "saadi", "shann", "sira", "thibra", "thyra", "vayla", "vyre", "vys", "wophyl", "wyn", "xiris", "xora", "yassa", "yries", "zita", "zys"]
        ]
    }
    
}