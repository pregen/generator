export let Elf = {
    name: 'Elf',
    weight: 14,
    ages: [
        { name: "Young Adult", min: 50, max: 100, weight: 55 },
        { name: "Early Mid-Age", min: 101, max: 300, weight: 25 },
        { name: "Late Mid-Age", min: 301, max: 600, weight: 13 },
        { name: "Elderly", min: 601, max: 750, weight: 2 }
    ],
    stats: [
        { stat: "str", weight: 2 },
        { stat: "dex", weight: 10, bonus: 2 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 2 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 5 }
    ],
    classes: [
        { name: "Barbarian", weight: 1 },
        { name: "Bard", weight: 1 },
        { name: "Cleric", weight: 5 },
        { name: "Druid", weight: 10 },
        { name: "Fighter", weight: 5 },
        { name: "Monk", weight: 10 },
        { name: "Paladin", weight: 5 },
        { name: "Ranger", weight: 10 },
        { name: "Rogue", weight: 10 },
        { name: "Sorcerer", weight: 10 },
        { name: "Warlock", weight: 5 },
        { name: "Wizard", weight: 5 }
    ],
    alignments: { 
        moral: [20, 10, 3], 
        societal: [5, 10, 20] 
    },
    names: {
        m: [
            ["Ad", "Ae", "Bal", "Bei", "Car", "Cra", "Dae", "Dor", "El", "Ela", "Er", "Far", "Fen", "Gen", "Glyn", "Hei", "Her", "Ian", "Ili", "Kea", "Kel", "Leo", "Lu", "Mira", "Mor", "Nae", "Nor", "Olo", "Oma", "Pa", "Per", "Pet", "Qi", "Qin", "Ralo", "Ro", "Sar", "Syl", "The", "Tra", "Ume", "Uri", "Va", "Vir", "Waes", "Wran", "Yel", "Yin", "Zin", "Zum"],
            ["balar", "beros", "can", "ceran", "dan", "dithas", "faren", "fir", "geiros", "golor", "hice", "horn", "jeon", "jor", "kas", "kian", "lamin", "lar", "len", "maer", "maris", "menor", "myar", "nan", "neiros", "nelis", "norin", "peiros", "petor", "qen", "quinal", "ran", "ren", "ric", "ris", "ro", "salor", "sandoral", "toris", "tumal", "valur", "ven", "warin", "wraek", "xalim", "xidor", "yarus", "ydark", "zeiros", "zumin"]
        ],
        f: [
            ["Ad", "Ara", "Bi", "Bry", "Cai", "Chae", "Da", "Dae", "Eil", "En", "Fa", "Fae", "Gil", "Gre", "Hele", "Hola", "Iar", "Ina", "Jo", "Key", "Kris", "Lia", "Lora", "Mag", "Mia", "Neri", "Ola", "Ori", "Phi", "Pres", "Qi", "Qui", "Rava", "Rey", "Sha", "Syl", "Tor", "Tris", "Ula", "Uri", "Val", "Ven", "Wyn", "Wysa", "Xil", "Xyr", "Yes", "Ylla", "Zin", "Zyl"],
            ["banise", "bella", "caryn", "cyne", "di", "dove", "fiel", "fina", "gella", "gwyn", "hana", "harice", "jyre", "kalyn", "krana", "lana", "lee", "leth", "lynn", "moira", "mys", "na", "nala", "phine", "phyra", "qirelle", "ra", "ralei", "rel", "rie", "rieth", "rona", "rora", "roris", "satra", "stina", "sys", "thana", "thyra", "tris", "varis", "vyre", "wenys", "wynn", "xina", "xisys", "ynore", "yra", "zana", "zorwyn"]
        ]
    }
}