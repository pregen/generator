export let HalfOrc = {

    name: 'Half-Orc',
    weight: 12,

    ages: [
        { name: "Adolescent", min: 9, max: 14, weight: 5 },
        { name: "Adult", min: 15, max: 45, weight: 90 },
        { name: "Elderly", min: 46, max: 60, weight: 5 }
    ],

    stats: [
        { stat: "str", weight: 20, bonus: 2 },
        { stat: "dex", weight: 6 },
        { stat: "con", weight: 10, bonus: 1 },
        { stat: "int", weight: 2 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 6 }
    ],

    classes: [
        { name: "Barbarian", weight: 20 },
        { name: "Bard", weight: 1 },
        { name: "Cleric", weight: 3 },
        { name: "Druid", weight: 5 },
        { name: "Fighter", weight: 20 },
        { name: "Monk", weight: 1 },
        { name: "Paladin", weight: 15 },
        { name: "Ranger", weight: 10 },
        { name: "Rogue", weight: 1 },
        { name: "Sorcerer", weight: 3 },
        { name: "Warlock", weight: 3 },
        { name: "Wizard", weight: 1 }
    ],

    alignments: { 
        moral: [2, 20, 20], 
        societal: [3, 10, 20] 
    },

    names: {
        m: [
            ["Ag", "Agg", "Ar", "Arn", "As", "At", "Atr", "B", "Bar", "Bel", "Bor", "Br", "Brak", "C", "Cr", "D", "Dor", "Dr", "Dur", "G", "Gal", "Gan", "Gar", "Gna", "Gor", "Got", "Gr", "Gram", "Grim", "Grom", "Grum", "Gul", "H", "Hag", "Han", "Har", "Hog", "Hon", "Hor", "Hun", "Hur", "K", "Kal", "Kam", "Kar", "Kel", "Kil", "Kom", "Kor", "Kra", "Kru", "Kul", "Kur", "Lum", "M", "Mag", "Mahl", "Mak", "Mal", "Mar", "Mog", "Mok", "Mor", "Mug", "Muk", "Mura", "N", "Oggu", "Ogu", "Ok", "Oll", "Or", "Rek", "Ren", "Ron", "Rona", "S", "Sar", "Sor", "T", "Tan", "Th", "Thar", "Ther", "Thr", "Thur", "Trak", "Truk", "Ug", "Uk", "Ukr", "Ull", "Ur", "Urth", "Urtr", "Z", "Za", "Zar", "Zas", "Zav", "Zev", "Zor", "Zur", "Zus"],
            ["a", "a", "a", "o", "o", "e", "i", "u", "u", "u"],
            ["bak", "bar", "bark", "bash", "bur", "burk", "d", "dak", "dall", "dar", "dark", "dash", "dim", "dur", "durk", "g", "gak", "gall", "gar", "gark", "gash", "glar", "gul", "gur", "m", "mak", "mar", "marsh", "mash", "mir", "mur", "n", "nar", "nars", "nur", "rak", "rall", "rash", "rim", "rimm", "rk", "rsh", "rth", "ruk", "sk", "tar", "tir", "tur", "z", "zall", "zar", "zur"]
        ],
        f: [
            ["Al", "Ar", "Br", "Ek", "El", "Fal", "Fel", "Fol", "Ful", "G", "Gaj", "Gar", "Gij", "Gor", "Gr", "Gry", "Gyn", "Hur", "K", "Kar", "Kat", "Ker", "Ket", "Kir", "Kot", "Kur", "Kut", "Lag", "M", "Mer", "Mir", "Mor", "N", "Ol", "Oot", "Puy", "R", "Rah", "Rahk", "Ras", "Rash", "Raw", "Roh", "Rohk", "S", "Sam", "San", "Sem", "Sen", "Sh", "Shay", "Sin", "Sum", "Sun", "Tam", "Tem", "Tu", "Tum", "Ub", "Um", "Ur", "Van", "Zan", "Zen", "Zon", "Zun"],
            ["a", "a", "o", "o", "e", "i", "i", "u"],
            ["d", "da", "dar", "dur", "g", "gar", "gh", "gri", "gu", "sh", "sha", "shi", "gum", "gume", "gur", "ki", "mar", "mi", "mira", "me", "mur", "ne", "ner", "nir", "nar", "nchu", "ni", "nur", "ral", "rel", "ri", "rook", "ti", "tah", "tir", "tar", "tur", "war", "z", "zar", "zara", "zi", "zur", "zura", "zira"]
        ]
    }
}