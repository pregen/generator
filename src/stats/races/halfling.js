export let Halfling = {

    name: 'Halfling',
    weight: 10,
    
    ages: [
        { name: "Adolescent", min: 10, max: 20, weight: 5 },
        { name: "Adult", min: 21, max: 65, weight: 60 },
        { name: "Middle Aged", min: 66, max: 120, weight: 30 },
        { name: "Elderly", min: 121, max: 150, weight: 5 }
    ],
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 10, bonus: 2 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 3 },
        { stat: "wis", weight: 5 },
        { stat: "cha", weight: 3 }
    ],
    
    classes: [
        { name: "Barbarian", weight: 1 },
        { name: "Bard", weight: 10 },
        { name: "Cleric", weight: 10 },
        { name: "Druid", weight: 10 },
        { name: "Fighter", weight: 1 },
        { name: "Monk", weight: 5 },
        { name: "Paladin", weight: 1 },
        { name: "Ranger", weight: 5 },
        { name: "Rogue", weight: 25 },
        { name: "Sorcerer", weight: 1 },
        { name: "Warlock", weight: 1 },
        { name: "Wizard", weight: 1 }
    ],
    
    alignments: { 
        moral: [20, 10, 3],
        societal: [20, 10, 3] 
    },
    
    names: {
        m: [
            ["An", "Ar", "Bar", "Bel", "Con", "Cor", "Dan", "Dav", "El", "Er", "Fal", "Fin", "Flyn", "Gar", "Go", "Hal", "Hor", "Ido", "Ira", "Jan", "Jo", "Kas", "Kor", "La", "Lin", "Mar", "Mer", "Ne", "Nor", "Ori", "Os", "Pan", "Per", "Pim", "Quin", "Quo", "Ri", "Ric", "San", "Shar", "Tar", "Te", "Ul", "Uri", "Val", "Vin", "Wen", "Wil", "Xan", "Xo", "Yar", "Yen", "Zal", "Zen"],
            ["ace", "amin", "bin", "bul", "dak", "dal", "der", "don", "emin", "eon", "fer", "fire", "gin", "hace", "horn", "kas", "kin", "lan", "los", "min", "mo", "nad", "nan", "ner", "orin", "os", "pher", "pos", "ras", "ret", "ric", "rich", "rin", "ry", "ser", "sire", "ster", "ton", "tran", "umo", "ver", "vias", "von", "wan", "wrick", "yas", "yver", "zin", "zor", "zu"]
        ],
        f: [
            ["An", "Ari", "Bel", "Bre", "Cal", "Chen", "Dar", "Dia", "Ei", "Eo", "Eli", "Era", "Fay", "Fen", "Fro", "Gel", "Gra", "Ha", "Hil", "Ida", "Isa", "Jay", "Jil", "Kel", "Kith", "Le", "Lid", "Mae", "Mal", "Mar", "Ne", "Ned", "Odi", "Ora", "Pae", "Pru", "Qi", "Qu", "Ri", "Ros", "Sa", "Shae", "Syl", "Tham", "Ther", "Tryn", "Una", "Uvi", "Va", "Ver", "Wel", "Wi", "Xan", "Xi", "Yes", "Yo", "Zef", "Zen"],
            ["alyn", "ara", "brix", "byn", "caryn", "cey", "da", "dove", "drey", "elle", "eni", "fice", "fira", "grace", "gwen", "haly", "jen", "kath", "kis", "leigh", "la", "lie", "lile", "lienne", "lyse", "mia", "mita", "ne", "na", "ni", "nys", "ola", "ora", "phina", "prys", "rana", "ree", "ri", "ris", "sica", "sira", "sys", "tina", "trix", "ula", "vira", "vyre", "wyn", "wyse", "yola", "yra", "zana", "zira"]
        ]
    }
    
}