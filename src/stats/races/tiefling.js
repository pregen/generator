export let Tiefling = {

    name: 'Tiefling',
    weight: 4,

    ages: [
        { name: "Adolescent", min: 12, max: 18, weight: 5 },
        { name: "Adult", min: 19, max: 45, weight: 65 },
        { name: "Middle Aged", min: 46, max: 60, weight: 25 },
        { name: "Elderly", min: 61, max: 90, weight: 5 }
    ],
    
    stats: [
        { stat: "str", weight: 2 },
        { stat: "dex", weight: 1 },
        { stat: "con", weight: 2 },
        { stat: "int", weight: 6, bonus: 1 },
        { stat: "wis", weight: 1 },
        { stat: "cha", weight: 8, bonus: 2 }
    ],
    
    classes: [
        { name: "Barbarian", weight: 5 },
        { name: "Bard", weight: 1 },
        { name: "Cleric", weight: 8 },
        { name: "Druid", weight: 5 },
        { name: "Fighter", weight: 5 },
        { name: "Monk", weight: 10 },
        { name: "Paladin", weight: 5 },
        { name: "Ranger", weight: 1 },
        { name: "Rogue", weight: 1 },
        { name: "Sorcerer", weight: 10 },
        { name: "Warlock", weight: 10 },
        { name: "Wizard", weight: 5 }
    ],
    
    alignments: { 
        moral: [20, 40, 5], 
        societal: [3, 10, 20] 
    },
    
    names: {
        m: [
            ["Aet", "Ak", "Am", "Aran", "And", "Ar", "Ark", "Bar", "Car", "Cas", "Dam", "Dhar", "Eb", "Ek", "Er", "Gar", "Gu", "Gue", "Hor", "Ia", "Ka", "Kai", "Kar", "Kil", "Kos", "Ky", "Loke", "Mal", "Male", "Mav", "Me", "Mor", "Neph", "Oz", "Ral", "Re", "Rol", "Sal", "Sha", "Sir", "Ska", "The", "Thy", "Thyne", "Ur", "Uri", "Val", "Xar", "Zar", "Zer", "Zher", "Zor"],
            ["adius", "akas", "akos", "char", "cis", "cius", "dos", "emon", "ichar", "il", "ilius", "ira", "lech", "lius", "lyre", "marir", "menos", "meros", "mir", "mong", "mos", "mus", "non", "rai", "rakas", "rakir", "reus", "rias", "ris", "rius", "ron", "ros", "rus", "rut", "shoon", "thor", "thos", "thus", "us", "venom", "vir", "vius", "xes", "xik", "xikas", "xire", "xius", "xus", "zer", "zire"]
        ],
        f: [
            ["Af", "Agne", "Ani", "Ara", "Ari", "Aria", "Bel", "Bri", "Cre", "Da", "Di", "Dim", "Dor", "Ea", "Fri", "Gri", "His", "In", "Ini", "Kal", "Le", "Lev", "Lil", "Ma", "Mar", "Mis", "Mith", "Na", "Nat", "Ne", "Neth", "Nith", "Ori", "Pes", "Phe", "Qu", "Ri", "Ro", "Sa", "Sar", "Seiri", "Sha", "Val", "Vel", "Ya", "Yora", "Yu", "Za", "Zai", "Ze"],
            ["bis", "borys", "cria", "cyra", "dani", "doris", "faris", "firith", "goria", "grea", "hala", "hiri", "karia", "ki", "laia", "lia", "lies", "lista", "lith", "loth", "lypsis", "lyvia", "maia", "meia", "mine", "narei", "nirith", "nise", "phi", "pione", "punith", "qine", "rali", "rissa", "seis", "solis", "spira", "tari", "tish", "uphis", "vari", "vine", "wala", "wure", "xibis", "xori", "yis", "yola", "za", "zis"]
        ]
    }
    
}