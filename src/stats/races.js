import { Dragonborn } from "./races/dragonborn"
import { Dwarf }      from "./races/dwarf"
import { Elf }        from "./races/elf"
import { Gnome }      from "./races/gnome"
import { HalfElf }    from "./races/halfElf"
import { Halfling }   from "./races/halfling"
import { HalfOrc }    from "./races/halfOrc"
import { Human }      from "./races/human"
import { Tiefling }   from "./races/tiefling"

export let races = {
    Dragonborn: Dragonborn,
    Dwarf: Dwarf,
    Elf: Elf,
    Gnome: Gnome,
    HalfElf: HalfElf,
    Halfling: Halfling,
    HalfOrc: HalfOrc,
    Human: Human,
    Tiefling: Tiefling
}