import { races } from "./races"
import { classes } from "./classes"
import { backgrounds } from "./backgrounds"
import { genders } from "./genders"

export let stats = {
	races: races,
	classes: classes,
	backgrounds: backgrounds,
	genders: genders
}