export let genders = [
    { letter: "m", name: "Male",   weight: 63 },
    { letter: "f", name: "Female", weight: 37 }
];