export let Druid = {
    
    name: 'Druid',
    
    alignments: {
        moral: [45, 45, 10],
        societal: [10, 30, 60]
    },
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 3 },
        { stat: "con", weight: 3 },
        { stat: "int", weight: 5 },
        { stat: "wis", weight: 10 },
        { stat: "cha", weight: 1 }
    ]
    
}

