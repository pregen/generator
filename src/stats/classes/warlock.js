export let Warlock = {
    
    name: 'Warlock',
    
    alignments: {
        moral: [35, 50, 15],
        societal: [25, 35, 40]
    },
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 1 },
        { stat: "con", weight: 3 },
        { stat: "int", weight: 3 },
        { stat: "wis", weight: 10 },
        { stat: "cha", weight: 5 }
    ]
    
}