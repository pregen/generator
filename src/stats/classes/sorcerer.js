export let Sorcerer = {
    
    name: 'Sorcerer',
    
    alignments: {
        moral: [35, 50, 15],
        societal: [30, 30, 40]
    },
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 1 },
        { stat: "con", weight: 10 },
        { stat: "int", weight: 3 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 5 }
    ]
    
}