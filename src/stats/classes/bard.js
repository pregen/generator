export let Bard = {
    
    name: 'Bard',
    
    alignments: {
        moral: [47, 47, 6],
        societal: [20, 35, 45]
    },
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 3 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 3 },
        { stat: "wis", weight: 5 },
        { stat: "cha", weight: 10 }
    ]
    
}