export let Cleric = {

    name: 'Cleric',

    alignments: { 
        moral: [55, 40, 5], 
        societal: [55, 35, 10]
    },

    stats: [
        { stat: "str", weight: 3 },
        { stat: "dex", weight: 5 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 1 },
        { stat: "wis", weight: 10 },
        { stat: "cha", weight: 3 }
    ]
    
}