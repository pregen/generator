export let Rogue = {
    
    name: 'Rogue',
    
    alignments: {
        moral: [43, 53, 4],
        societal: [1, 24, 75]
    },
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 10 },
        { stat: "con", weight: 3 },
        { stat: "int", weight: 5 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 1 }
    ]
    
}