export let Fighter = {
    
    name: 'Fighter',
    
    alignments: {
        moral: [40, 50, 10],
        societal: [20, 30, 50]
    },
    
    stats: [
        { stat: "str", weight: 3 },
        { stat: "dex", weight: 10 },
        { stat: "con", weight: 5 },
        { stat: "int", weight: 1 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 1 }
    ]
    
}

