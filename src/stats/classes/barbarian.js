export let Barbarian = {

    name: 'Barbarian',

    alignments: {
        moral: [30, 40, 30],
        societal: [10, 35, 55]
    },

    stats: [
        { stat: "str", weight: 10 },
        { stat: "dex", weight: 3 },
        { stat: "con", weight: 5 },
        { stat: "int", weight: 1 },
        { stat: "wis", weight: 1 },
        { stat: "cha", weight: 3 }
    ]
    
}