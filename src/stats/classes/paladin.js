export let Paladin = {
    
    name: 'Paladin',
    
    alignments: {
        moral: [58, 38, 2],
        societal: [70, 20, 10]
    },
    
    stats: [
        { stat: "str", weight: 10 },
        { stat: "dex", weight: 1 },
        { stat: "con", weight: 3 },
        { stat: "int", weight: 1 },
        { stat: "wis", weight: 5 },
        { stat: "cha", weight: 4 }
    ]
    
}
