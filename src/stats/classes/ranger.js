export let Ranger = {
    
    name: 'Ranger',
    
    alignments: {
        moral: [38, 58, 2],
        societal: [25, 30, 45]
    },
    
    stats: [
        { stat: "str", weight: 5 },
        { stat: "dex", weight: 10 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 3 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 1 }
    ]
    
}