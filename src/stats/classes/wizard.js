export let Wizard = {
    
    name: 'Wizard',
    
    alignments: {
        moral: [50, 45, 5],
        societal: [40, 35, 25]
    },
    
    stats: [
        { stat: "str", weight: 1 },
        { stat: "dex", weight: 1 },
        { stat: "con", weight: 3 },
        { stat: "int", weight: 10 },
        { stat: "wis", weight: 5 },
        { stat: "cha", weight: 3 }
    ]
    
}
