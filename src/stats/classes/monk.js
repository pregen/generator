export let Monk = {
    
    name: 'Monk',
    
    alignments: {
        moral: [50, 40, 10],
        societal: [40, 35, 25]
    },
    
    stats: [
        { stat: "str", weight: 5 },
        { stat: "dex", weight: 10 },
        { stat: "con", weight: 1 },
        { stat: "int", weight: 3 },
        { stat: "wis", weight: 3 },
        { stat: "cha", weight: 1 }
    ]
    
}

