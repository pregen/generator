import { Barbarian } from "./classes/barbarian"
import { Bard }      from "./classes/bard"
import { Cleric }    from "./classes/cleric"
import { Druid }     from "./classes/druid"
import { Fighter }   from "./classes/fighter"
import { Monk }      from "./classes/monk"
import { Paladin }   from "./classes/paladin"
import { Ranger }    from "./classes/ranger"
import { Rogue }     from "./classes/rogue"
import { Sorcerer }  from "./classes/sorcerer"
import { Warlock }   from "./classes/warlock"
import { Wizard }    from "./classes/wizard"

export let classes = {
    Barbarian: Barbarian,
    Bard: Bard,
    Cleric: Cleric,
    Druid: Druid,
    Fighter: Fighter,
    Monk: Monk,
    Paladin: Paladin,
    Ranger: Ranger,
    Rogue: Rogue,
    Sorcerer: Sorcerer,
    Warlock: Warlock,
    Wizard: Wizard
}