import { character } from "./logic/character"

function insertCharacters(num_chars) {
    for (let i = 0; i < num_chars; i++) {
        
        const c = character.generate();

        let stats = '';

        for (let statid in c.stats.calc) {
            const theStat = c.stats.calc[statid];
            stats += '<div class="stat" title="' + theStat.score.roll + ' + ' + theStat.score.bonus + ' ('+theStat.score.points+')"><div class="statname">' + theStat.score.name + '</div>' + theStat.score.score + '</div>';
        }

        const row = 
              '<article class="char"><section class="name">' + c.name + '</section>'
            + '<section class="infoblock">'
            + '<div class="rc">' + c.race.name + ' ' + c.class.name + '</div>'
            + '<div class="alignment">' + c.alignment.name + '</div>'
            + '<div class="gender">' + c.gender.name + ', ' + c.age.years + ' <span class="agegroup">('+c.age.group+')</span></div>'
            + '<div class="background">' + c.background + '</div>'
            + '</section>'
            + '<section class="statblock">'
            + '<div class="score">' + c.stats.raw.points + '</div>'
             + stats + '</section></article>';

        document
            .getElementById('characters')
            .innerHTML += row;
    }
}

insertCharacters(10);

document
    .getElementById('moar')
    .addEventListener('click', e => { 
        e.preventDefault(); 
        insertCharacters(10);
    });
