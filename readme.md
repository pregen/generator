# Pregen Generator

This is a very-simple system aimed at helping you get some good 5e-style pregen stubs quickly. Every 
character stub produced by this application is entirely randomly-generated.

## Building the Application

There's no need to build it if you just want some pregens! Just go to the [website](http://charactergen.cmillr.com)
and start generating.  However, if you'd like to tinker with it or contribute, then please feel free!

This application uses some Node.js tools to build a static web application; you don't need node
running on your computer or server to operate the application, you only need node to build it 
from source.  Once built, it operates as a static HTML/JS web page.

1. If you don't have Node installed, now would be the time to download the node
installer for your environment.

2. Once you have node installed, [download the source code](https://bitbucket.org/pregen/generator/get/master.zip) 
or [clone it](https://bitbucket.org/pregen/generator) from Bitbucket.

3. Install node dependencies by running `npm install` in your project directory.

4. Build the application by running `npm run-script build` in your project directory.

5. Once built, you can open the file `public/index.html` in your browser.  Just 
double-click that file, and the whole application will run in your browser (no internet needed.)

## Randomness in Character Generation

The characters are mostly randomly generated using a kind of a weighting system.

In general, the various properties of each character are selected at random based not simply on even
statistical chance, but on a system that gives more weight to some options than others (and will
therefore produce those more-weighted selections more often than others.)

Here are some examples of where weight and other factors change the way characters are generated:

1. It's slightly more likely to produce a male character than a female character.  This reflects
the gender ratio of the characters played by the folks in my own gaming group.
2. The attribute scores are generated using the standard "roll 4d6 and drop the worst one" method.
Even so, I was seeing a few more extra-low stats (3 or 4) than I was imagining I would see.  To 
make up for this, I changed the stat-generator to start over from scratch if the four dice together 
add up to 6 or less (i.e., 1s and 2s).  This has the effect of eliminaing almost all the extra-low 
stats (3-4-5), but you'll still sometimes see 6s and frequently 7s.
3. The race selection is weighted, meaning certain races (humans, dwarves, elves) will come up 
more often, and certain other races (dragonborn, tiefling) will come up less often.
4. Any race can produce a character of any class, but a given race will produce certain classes
much more often, and certain classes much less often.
5. Both race and class affect the statistics behind alignment; right now class has a much larger 
effect on the statistics behind alignment selection than race does.  I made it evenly-balanced at
first, but that ended up producing some pretty weird results (i.e., lots of chaotic evil paladins, 
for example.)
6. Character age is typically chosen from 3 to 5 race-based age segments, which are heavily 
weighted toward young-adult and then middle-aged characters, with youths and elderly characters 
appearing only rarely.

## Improving the Generator

I'm not 100% happy with the weights on the different charcter properties; it still feels like the
system generates too many super-standard or very-weird characters.  It doesn't consistently
produce characters that are both non-tropey and that *also* make story sense.  This would be a
great area for someone to pitch in and improve.

The name generator is *awesome* but the source data is blatantly ripped off from someone else's 
website.  It would be nice to either come up with a new set of name-generation data, or to get
permission from the original creator to use his name generation information here. 

Another area would be to perhaps assign stat scores to the six attributes, and to assign race 
and class modifiers to those stats automatically (since right now the generator doesn't do that.)
I'd imagine for each race and each class, we'd want each stat to have a 'weight', and we can then
use our weighted random-chooser to determine which rolled stats go with which attributes.

There are a lot of other areas which need improvement here! If you'd like to be involved, check out
the source code, start tinkering, and send some pull requests!
