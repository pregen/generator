module.exports = {
    entry: './src/app.js',
    output: {
        filename: './public/app.js'
    },
    module: {
        rules: [
            { 
                test: /\.js/, 
                exclude: /node_modules/, 
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["babel-preset-env"]
                    }
                }
            }
        ]
    }
}