# TO DO:

- Format characters better, to show more info
- ~~Boil characters down to a number (hexadecimal?) that can be permalinked~~ Moved to [Issue #1](https://bitbucket.org/pregen/generator/issues/1/idea-make-characters-bookmarkable)
- Generate hit points
- Generate proficiencies
- Generate background details (trait, ideal, bond, flaw - and proficiencies)
- Generate sub-races and sub-classes
- Generate characters above level 1
- Toggle NPCs based on CR versus Pregen PCs based on Point-Buy Points
- Allow range selection of PC Point-buy points (or NPC CR)
- A better multi-item weighting system (letter or word based?) that gives more 
  weight to complementary characteristics -- when a characteristic is chosen,
  then complementary ones should be more likey later.  Each item ought to be
  able to have a 'groupings: []' property that lists the names of groupings
  to which it belongs, and the per-character generator should alter weightings
  based on the 'groupings' of items already chosen (maybe multiply them by 
  the count of groupings +1, so an already-chosen grouping would x2, two 
  would x3 etc)